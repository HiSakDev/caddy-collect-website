FROM caddy:2.7.6-builder AS builder

ARG CADDY_COLLECT_WEBSITE_VERSION=main
RUN xcaddy build \
    --with gitlab.com/HiSakDev/caddy-collect-website@$CADDY_COLLECT_WEBSITE_VERSION

FROM caddy:2.7.6

COPY --from=builder /usr/bin/caddy /usr/bin/caddy
