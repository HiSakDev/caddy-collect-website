package caddy_collect_website

import (
	"context"
	"encoding/json"
	"fmt"
	"os"
	"path/filepath"
	"reflect"
	"strconv"
	"strings"
	"time"

	"github.com/caddyserver/caddy/v2"
	"github.com/caddyserver/caddy/v2/caddyconfig"
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"github.com/robfig/cron/v3"
	"go.uber.org/zap"

	"gitlab.com/HiSakDev/caddy-collect-website/internal"
)

var (
	scheduler *internal.Scheduler
)

func init() {
	if b, _ := strconv.ParseBool(os.Getenv("CADDY_COLLECT_WEBSITE_SCHEDULER_WITH_SECONDS")); b {
		scheduler = internal.NewScheduler(cron.WithSeconds())
	} else {
		scheduler = internal.NewScheduler()
	}
	caddy.RegisterModule(new(CollectWebsiteApp))
	httpcaddyfile.RegisterGlobalOption(moduleName, parseCaddyfileGlobalOption)
}

func parseCaddyfileGlobalOption(d *caddyfile.Dispenser, _ any) (any, error) {
	app := new(CollectWebsiteApp)
	if err := app.UnmarshalCaddyfile(d); err != nil {
		return nil, err
	}
	return httpcaddyfile.App{
		Name:  moduleName,
		Value: caddyconfig.JSON(app, nil),
	}, nil
}

type CollectProvider interface {
	UnmarshalCaddyfile(*caddyfile.Dispenser) error
	Start(context.Context) error
	Stop(bool) error
	Apply()
}

type CollectWebsiteApp struct {
	provider       CollectProvider
	root, schedule string
	keep           []int64

	current, previous string
	isExit            bool
	logger            *zap.Logger
}

func (cwa *CollectWebsiteApp) MarshalJSON() ([]byte, error) {
	return json.Marshal(struct {
		Provider internal.InterfaceMarshal `json:"provider,omitempty"`
		Root     string                    `json:"root,omitempty"`
		Schedule string                    `json:"schedule,omitempty"`
		Keep     []int64                   `json:"keep,omitempty"`
	}{
		Provider: internal.InterfaceMarshal{
			Type:  reflect.TypeOf(cwa.provider).String(),
			Value: cwa.provider,
		},
		Root:     cwa.root,
		Schedule: cwa.schedule,
		Keep:     cwa.keep,
	})
}

func (cwa *CollectWebsiteApp) UnmarshalJSON(b []byte) error {
	v := new(struct {
		Provider internal.InterfaceUnmarshal `json:"provider,omitempty"`
		Root     string                      `json:"root,omitempty"`
		Schedule string                      `json:"schedule,omitempty"`
		Keep     []int64                     `json:"keep,omitempty"`
	})
	if err := json.Unmarshal(b, v); err != nil {
		return err
	}
	switch v.Provider.Type {
	case reflect.TypeOf(new(internal.S3Provider)).String():
		cwa.provider = internal.NewS3Provider(cwa)
		if err := json.Unmarshal(v.Provider.Value, cwa.provider); err != nil {
			return err
		}
	default:
		return fmt.Errorf("unexpected provider: %s", v.Provider.Type)
	}
	cwa.root = v.Root
	cwa.schedule = v.Schedule
	cwa.keep = v.Keep
	return nil
}

func (cwa *CollectWebsiteApp) CaddyModule() caddy.ModuleInfo {
	return caddy.ModuleInfo{
		ID:  moduleName,
		New: func() caddy.Module { return new(CollectWebsiteApp) },
	}
}

func (cwa *CollectWebsiteApp) UnmarshalCaddyfile(d *caddyfile.Dispenser) error {
	if !d.Next() {
		return d.ArgErr()
	}
	for nesting := d.Nesting(); d.NextBlock(nesting); {
		option := d.Val()
		switch option {
		case "provider":
			t := "s3"
			d.AllArgs(&t)
			switch t {
			case "s3":
				cwa.provider = internal.NewS3Provider(cwa)
				if err := cwa.provider.UnmarshalCaddyfile(d); err != nil {
					return err
				}
			default:
				return d.Errf("unexpected provider: %s", t)
			}
		case "root":
			if !d.AllArgs(&cwa.root) {
				return d.ArgErr()
			}
		case "schedule":
			cwa.schedule = strings.Join(d.RemainingArgs(), " ")
		case "keep":
			for i, s := range d.RemainingArgs() {
				switch i {
				case 0, 1:
					if n, err := strconv.ParseInt(s, 10, 64); err != nil {
						return d.WrapErr(err)
					} else {
						cwa.keep = append(cwa.keep, n)
					}
				case 2:
					if n, err := time.ParseDuration(s); err != nil {
						return d.WrapErr(err)
					} else {
						cwa.keep = append(cwa.keep, n.Nanoseconds())
					}
				}
			}
		default:
			return d.Errf("unexpected directive: %s", option)
		}
	}
	if cwa.provider == nil {
		return d.Err("empty provider")
	}
	if cwa.root == "" {
		return d.Err("empty root dirs")
	}
	return nil
}

func (cwa *CollectWebsiteApp) Start() error {
	if err := scheduler.Stop(); err != nil {
		return err
	}
	if err := cwa.provider.Start(scheduler.Context()); err != nil {
		return err
	}
	return scheduler.Start(cwa.schedule, cwa.provider.Apply)
}

func (cwa *CollectWebsiteApp) Stop() error {
	cwa.isExit = caddy.Exiting()
	if cwa.isExit {
		if scheduler != nil {
			cwa.Log().Info("exit scheduler", zap.Error(scheduler.Stop()))
		}
	}
	return cwa.provider.Stop(cwa.isExit)
}

func (cwa *CollectWebsiteApp) Root() string {
	return cwa.root
}

func (cwa *CollectWebsiteApp) Publish() string {
	if cwa.current == "" {
		return ""
	}
	return filepath.Join(cwa.Root(), cwa.current)
}

func (cwa *CollectWebsiteApp) PreviousFile(name string) string {
	if cwa.previous == "" {
		return ""
	}
	return filepath.Join(cwa.Root(), cwa.previous, name)
}

func (cwa *CollectWebsiteApp) Fetch(name string) {
	cwa.previous = name
	cwa.Log().Info("update fetch", zap.String("previous", cwa.previous), zap.String("current", cwa.current))
}

func (cwa *CollectWebsiteApp) Apply(name string) {
	cwa.current = name
	cwa.Log().Info("update publish", zap.String("previous", cwa.previous), zap.String("current", cwa.current))
}

func (cwa *CollectWebsiteApp) KeepRules() (merge int, publish int, delay time.Duration) {
	n := len(cwa.keep)
	switch {
	case n > 2:
		delay = time.Duration(cwa.keep[2])
		fallthrough
	case n > 1:
		if cwa.keep[1] >= 0 {
			publish = int(cwa.keep[1])
		}
		fallthrough
	case n > 0:
		merge = int(cwa.keep[0])
	}
	return
}

func (cwa *CollectWebsiteApp) Log() *zap.Logger {
	if cwa.logger == nil {
		cwa.logger = caddy.Log().Named(moduleName)
	}
	return cwa.logger
}
