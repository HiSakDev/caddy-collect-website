package internal

import (
	"context"
	"sync"
	"sync/atomic"

	"github.com/robfig/cron/v3"
)

type Scheduler struct {
	*cron.Cron
	wg      sync.WaitGroup
	ctx     context.Context
	cancel  context.CancelFunc
	running atomic.Bool
}

func NewScheduler(opts ...cron.Option) *Scheduler {
	s := new(Scheduler)
	s.Cron = cron.New(opts...)
	s.ctx, s.cancel = context.WithCancel(context.Background())
	return s
}

func (s *Scheduler) Context() context.Context {
	return s.ctx
}

func (s *Scheduler) Start(spec string, cmd func()) error {
	f := func() {
		if !s.running.CompareAndSwap(false, true) {
			return
		}
		defer s.running.Store(false)
		s.wg.Add(1)
		defer s.wg.Done()
		cmd()
	}
	if spec != "" {
		if _, err := s.Cron.AddFunc(spec, f); err != nil {
			return err
		}
	}
	go f()
	if spec != "" {
		s.Cron.Start()
	}
	return nil
}

func (s *Scheduler) Stop() error {
	s.cancel()
	s.ctx, s.cancel = context.WithCancel(context.Background())
	<-s.Cron.Stop().Done()
	s.wg.Wait()
	for _, entry := range s.Cron.Entries() {
		s.Cron.Remove(entry.ID)
	}
	return nil
}
