package internal

import (
	"context"
	"errors"
	"fmt"
	"net/http"
	"os"
	"path/filepath"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go-v2/aws"
	"github.com/aws/aws-sdk-go-v2/config"
	"github.com/aws/aws-sdk-go-v2/feature/s3/manager"
	"github.com/aws/aws-sdk-go-v2/service/s3"
	"github.com/aws/aws-sdk-go-v2/service/s3/types"
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/docker/go-units"
	"go.uber.org/zap"
)

type S3Provider struct {
	*s3.Client `json:"-"`
	ctx        context.Context
	app        CaddyWebsiteApp
	downloader *manager.Downloader
	logger     *zap.Logger

	Prefix       string `json:"prefix,omitempty"`
	Bucket       string `json:"bucket,omitempty"`
	Region       string `json:"region,omitempty"`
	Profile      string `json:"profile,omitempty"`
	Endpoint     string `json:"endpoint,omitempty"`
	UsePathStyle bool   `json:"use_path_style,omitempty"`
	PartSize     int64  `json:"part_size,omitempty"`
}

func NewS3Provider(app CaddyWebsiteApp) *S3Provider {
	return &S3Provider{
		app:    app,
		logger: app.Log().With(zap.String("provider", "s3")),
	}
}

func (s3c *S3Provider) UnmarshalCaddyfile(d *caddyfile.Dispenser) error {
	for nesting := d.Nesting(); d.NextBlock(nesting); {
		option := d.Val()
		switch option {
		case "prefix":
			if !d.Args(&s3c.Prefix) {
				return d.ArgErr()
			}
		case "bucket":
			if !d.Args(&s3c.Bucket) {
				return d.ArgErr()
			}
		case "region":
			if !d.Args(&s3c.Region) {
				return d.ArgErr()
			}
		case "profile":
			if !d.Args(&s3c.Profile) {
				return d.ArgErr()
			}
		case "endpoint":
			if !d.Args(&s3c.Endpoint) {
				return d.ArgErr()
			}
		case "use_path_style":
			s3c.UsePathStyle = true
		case "part_size":
			if size, err := units.RAMInBytes(strings.Join(d.RemainingArgs(), " ")); err != nil {
				return d.WrapErr(err)
			} else {
				s3c.PartSize = size
			}
		default:
			return d.Errf("unexpected directive: %s", option)
		}
	}
	return nil
}

func (s3c *S3Provider) Start(ctx context.Context) error {
	s3c.ctx = ctx

	var configOpts []func(*config.LoadOptions) error
	if s3c.Region != "" {
		configOpts = append(configOpts, config.WithRegion(s3c.Region))
	}
	if s3c.Profile != "" {
		configOpts = append(configOpts, config.WithSharedConfigProfile(s3c.Profile))
	}

	cfg, err := config.LoadDefaultConfig(context.Background(), configOpts...)
	if err != nil {
		return err
	}
	s3c.Client = s3.NewFromConfig(cfg, func(o *s3.Options) {
		if s3c.Endpoint != "" {
			o.BaseEndpoint = aws.String(s3c.Endpoint)
		}
		o.UsePathStyle = s3c.UsePathStyle
	})
	s3c.downloader = manager.NewDownloader(s3c.Client, func(d *manager.Downloader) {
		d.PartSize = s3c.PartSize
	})
	return s3c.clean(true)
}

func (s3c *S3Provider) Stop(exit bool) error {
	if exit {
		s3c.logger.Info("exit client cleanup", zap.Error(s3c.clean(false)))
	}
	return nil
}

func (s3c *S3Provider) clean(init bool) error {
	dirs, err := filepath.Glob(filepath.Join(s3c.app.Root(), ".*"))
	if err != nil {
		return err
	}
	for _, dir := range dirs {
		s3c.logger.Info("prune download dirs", zap.String("dir", dir), zap.Error(os.RemoveAll(dir)))
	}
	if init {
		if dirs, err = filepath.Glob(filepath.Join(s3c.app.Root(), "fetch.*")); err != nil {
			return err
		}
		if len(dirs) > 0 {
			s3c.app.Fetch(filepath.Base(dirs[len(dirs)-1]))
		}
		if dirs, err = filepath.Glob(filepath.Join(s3c.app.Root(), "apply.*")); err != nil {
			return err
		}
		if len(dirs) > 0 {
			s3c.app.Apply(filepath.Base(dirs[len(dirs)-1]))
		}
	}
	return nil
}

func (s3c *S3Provider) Apply() {
	t := time.Now().Format("20060102T150405")
	logger := s3c.logger.With(zap.String("ts", t))
	saveDir := filepath.Join(s3c.app.Root(), fmt.Sprintf(".%s", t))
	if err := s3c.downloadAll(saveDir); err != nil {
		logger.Error("download failed", zap.Error(err), zap.NamedError("save", os.RemoveAll(saveDir)))
		return
	}
	merge, publish, delay := s3c.app.KeepRules()
	dirs, err := filepath.Glob(filepath.Join(s3c.app.Root(), "fetch.*"))
	if err != nil {
		logger.Error("glob fetch dirs failed", zap.Error(err))
		return
	}
	var fetchDir string
	applyDir := filepath.Join(s3c.app.Root(), fmt.Sprintf("apply.%s", t))
	keep := len(dirs) - 1 - merge
	for i, dir := range dirs {
		if i+1 > keep {
			if err = DirCopy(dir, applyDir); err != nil {
				logger.Error("copy apply dirs failed", zap.String("dir", dir), zap.Error(err),
					zap.NamedError("src", os.RemoveAll(dir)), zap.NamedError("dst", os.RemoveAll(applyDir)))
				s3c.app.Fetch("")
				return
			}
			fetchDir = dir
		} else {
			logger.Info("prune fetch dirs", zap.String("dir", dir), zap.Error(os.RemoveAll(dir)))
		}
	}
	if fetchDir == "" {
		s3c.app.Fetch(fetchDir)
	} else if _, err = os.Stat(fetchDir); err != nil {
		logger.Error("stat fetch dirs failed", zap.Error(err))
	} else {
		s3c.app.Fetch(filepath.Base(fetchDir))
	}
	if _, err = os.Stat(applyDir); err == nil {
		s3c.app.Apply(filepath.Base(applyDir))
	}

	<-time.After(delay)
	if dirs, err = filepath.Glob(filepath.Join(s3c.app.Root(), "apply.*")); err != nil {
		logger.Error("glob apply dirs failed", zap.Error(err))
		return
	}
	keep = len(dirs) - 1 - publish
	for i, dir := range dirs {
		if i+1 <= keep {
			logger.Info("prune apply dirs", zap.String("dir", dir), zap.Error(os.RemoveAll(dir)))
		}
	}
}

func (s3c *S3Provider) download(dir string, obj types.Object) error {
	relKey, err := filepath.Rel(s3c.Prefix, *obj.Key)
	if err != nil {
		return err
	}
	var mtime time.Time
	prevFile := s3c.app.PreviousFile(relKey)
	if prevFile != "" {
		if info, err := os.Stat(prevFile); err == nil {
			mtime = info.ModTime()
		}
	}
	key := filepath.Join(dir, relKey)
	if err = os.MkdirAll(filepath.Dir(key), 0755); err != nil {
		return err
	}
	f, err := os.Create(key)
	if err != nil {
		return err
	}
	defer f.Close()
	input := &s3.GetObjectInput{
		Bucket: aws.String(s3c.Bucket),
		Key:    obj.Key,
	}
	if !mtime.IsZero() {
		input.IfModifiedSince = &mtime
	}
	t := time.Now()
	n, err := s3c.downloader.Download(s3c.ctx, f, input)
	s3c.logger.Debug("download object", zap.String("file", key), zap.Int64("size", n),
		zap.String("elapsed", time.Now().Sub(t).String()), zap.Error(err))
	if err != nil {
		var statusErr interface{ HTTPStatusCode() int }
		if errors.As(err, &statusErr) && statusErr.HTTPStatusCode() == http.StatusNotModified && prevFile != "" {
			if err = os.Remove(key); err != nil {
				return err
			}
			return os.Link(prevFile, key)
		}
		return err
	}
	return os.Chtimes(key, *obj.LastModified, *obj.LastModified)
}

func (s3c *S3Provider) downloadAll(dir string) error {
	if err := os.MkdirAll(dir, 0755); err != nil {
		return err
	}
	paginator := s3.NewListObjectsV2Paginator(s3c, &s3.ListObjectsV2Input{
		Bucket: aws.String(s3c.Bucket),
		Prefix: aws.String(s3c.Prefix),
	})
	for paginator.HasMorePages() {
		page, err := paginator.NextPage(s3c.ctx)
		if err != nil {
			return err
		}
		for _, obj := range page.Contents {
			if err = s3c.download(dir, obj); err != nil {
				return err
			}
		}
	}
	return os.Rename(dir, filepath.Join(s3c.app.Root(), fmt.Sprintf("fetch%s", filepath.Ext(dir))))
}
