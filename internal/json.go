package internal

import "encoding/json"

type InterfaceMarshal struct {
	Type  string
	Value any
}

type InterfaceUnmarshal struct {
	Type  string
	Value json.RawMessage
}
