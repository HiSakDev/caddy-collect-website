package internal

import (
	"time"

	"go.uber.org/zap"
)

type CaddyWebsiteApp interface {
	Root() string
	Publish() string
	PreviousFile(string) string
	Fetch(string)
	Apply(string)
	KeepRules() (int, int, time.Duration)
	Log() *zap.Logger
}
