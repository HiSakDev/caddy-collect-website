package internal

import (
	"io/fs"
	"os"
	"path/filepath"
	"time"
)

type dirModTime struct {
	name    string
	modTime time.Time
}

func DirCopy(src, dst string) error {
	var dirList []dirModTime
	if err := filepath.WalkDir(src, func(name string, d fs.DirEntry, err error) error {
		if err != nil {
			return err
		}
		relName, err := filepath.Rel(src, name)
		if err != nil {
			return err
		}
		if d.IsDir() {
			info, err := d.Info()
			if err != nil {
				return err
			}
			dstName := filepath.Join(dst, relName)
			if err = os.Mkdir(dstName, info.Mode()); err != nil && !os.IsExist(err) {
				return err
			}
			dirList = append(dirList, dirModTime{name: dstName, modTime: info.ModTime()})
		} else if d.Type().IsRegular() {
			dstName := filepath.Join(dst, relName)
			if _, err = os.Stat(dstName); err == nil {
				if err = os.Remove(dstName); err != nil {
					return err
				}
			}
			if err = os.Link(name, dstName); err != nil {
				return err
			}
		}
		return nil
	}); err != nil {
		return err
	}
	for i := len(dirList) - 1; i >= 0; i-- {
		if err := os.Chtimes(dirList[i].name, dirList[i].modTime, dirList[i].modTime); err != nil {
			return err
		}
	}
	return nil
}
