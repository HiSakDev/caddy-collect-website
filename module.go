package caddy_collect_website

import (
	"errors"
	"fmt"
	"net/http"
	"os"
	"path"
	"path/filepath"
	"strconv"
	"strings"

	"github.com/caddyserver/caddy/v2"
	"github.com/caddyserver/caddy/v2/caddyconfig/caddyfile"
	"github.com/caddyserver/caddy/v2/caddyconfig/httpcaddyfile"
	"github.com/caddyserver/caddy/v2/modules/caddyhttp"
	"go.uber.org/zap"
)

const moduleName = "collect_website"

func init() {
	caddy.RegisterModule(new(CollectWebsiteMiddleware))
	httpcaddyfile.RegisterHandlerDirective(moduleName, parseCaddyfileHandlerDirective)
}

func parseCaddyfileHandlerDirective(h httpcaddyfile.Helper) (caddyhttp.MiddlewareHandler, error) {
	cwm := new(CollectWebsiteMiddleware)
	return cwm, cwm.UnmarshalCaddyfile(h.Dispenser)
}

type CollectWebsiteMiddleware struct {
	app       *CollectWebsiteApp
	skipIndex bool
	logger    *zap.Logger

	Prefix    string   `json:"prefix,omitempty"`
	Domain    []string `json:"domain,omitempty"`
	Index     []string `json:"index,omitempty"`
	Recursive int      `json:"recursive,omitempty"`
}

func (cwm *CollectWebsiteMiddleware) CaddyModule() caddy.ModuleInfo {
	return caddy.ModuleInfo{
		ID:  caddy.ModuleID(fmt.Sprintf("http.handlers.%s", moduleName)),
		New: func() caddy.Module { return new(CollectWebsiteMiddleware) },
	}
}

func (cwm *CollectWebsiteMiddleware) UnmarshalCaddyfile(d *caddyfile.Dispenser) error {
	if !d.Next() {
		return d.ArgErr()
	}
	for nesting := d.Nesting(); d.NextBlock(nesting); {
		option := d.Val()
		switch option {
		case "prefix":
			if !d.AllArgs(&cwm.Prefix) {
				return d.ArgErr()
			}
		case "domain":
			cwm.Domain = d.RemainingArgs()
		case "index":
			cwm.Index = d.RemainingArgs()
		case "recursive":
			var v string
			if !d.AllArgs(&v) {
				return d.ArgErr()
			}
			if n, err := strconv.Atoi(v); err != nil {
				return d.WrapErr(err)
			} else {
				cwm.Recursive = n
			}
		default:
			return d.Errf("unexpected directive: %s", option)
		}
	}
	return nil
}

func (cwm *CollectWebsiteMiddleware) Provision(ctx caddy.Context) error {
	ctxApp, err := ctx.App(moduleName)
	if err != nil {
		return err
	}
	cwm.app = ctxApp.(*CollectWebsiteApp)
	cwm.skipIndex = !(len(cwm.Index) > 0)
	cwm.logger = ctx.Logger()
	return nil
}

func (cwm *CollectWebsiteMiddleware) tryFiles(root string, req *http.Request) {
	if cwm.Recursive == 0 {
		return
	}
	u := strings.TrimSuffix(req.URL.Path, "/")
	for count := 0; u != "."; u = path.Dir(u) {
		if u == "/" {
			count = cwm.Recursive + 1
		} else {
			count++
		}
		info, err := os.Stat(filepath.Join(root, u))
		if err != nil {
			cwm.logger.Debug("not exists go to next", zap.String("uri", u), zap.Error(err))
		} else if info.IsDir() && !cwm.skipIndex {
			for _, index := range cwm.Index {
				if _, err = os.Stat(filepath.Join(root, u, index)); err == nil {
					req.URL.Path = path.Join(u, index)
					req.RequestURI = req.URL.RequestURI()
					cwm.logger.Debug("rewrite path", zap.String("uri", req.URL.Path))
					return
				}
			}
			cwm.logger.Debug("not exists go to next", zap.String("uri", u))
		} else {
			req.URL.Path = u
			req.RequestURI = req.URL.RequestURI()
			cwm.logger.Debug("rewrite path", zap.String("uri", req.URL.Path))
			break
		}
		if count > cwm.Recursive {
			break
		}
	}
}

func (cwm *CollectWebsiteMiddleware) ServeHTTP(res http.ResponseWriter, req *http.Request, next caddyhttp.Handler) error {
	root := cwm.app.Publish()
	if cwm.app.isExit {
		return caddyhttp.Error(http.StatusServiceUnavailable, errors.New("servers shutting down"))
	} else if root == "" {
		return caddyhttp.Error(http.StatusServiceUnavailable, errors.New("website not publish"))
	}
	ctx := req.Context()
	replacer := ctx.Value(caddy.ReplacerCtxKey).(*caddy.Replacer)
	var domain string
	for _, d := range cwm.Domain {
		if v, ok := replacer.GetString(strings.Trim(d, "{}")); ok && v != "" {
			domain = v
			break
		} else if domain = req.Header.Get(d); domain != "" {
			break
		}
	}
	if len(cwm.Domain) > 0 && domain == "" {
		return caddyhttp.Error(http.StatusBadRequest, errors.New("domain header not set"))
	}
	if vars, ok := ctx.Value(caddyhttp.VarsCtxKey).(map[string]any); ok {
		root = filepath.Join(root, domain, cwm.Prefix)
		vars["root"] = root
		cwm.tryFiles(root, req)
	}
	return next.ServeHTTP(res, req)
}
